package view.menue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.User;
import model.persistanceDB.AccessDB;
import model.persistanceDB.UserDB;

public class CreateUserMenu extends JFrame {
	
	private JPanel contentPane;
	private JTextField txtLoginName;
	private JTextField txtCity;
	private JTextField txtPostalCode;
	private JTextField txtHouseNumber;
	private JTextField txtStreet;
	private JTextField txtBirthday;
	private JTextField txtSurName;
	private JTextField txtFirstName;
	private JTextField txtSalaryExpectations;
	private JTextField txtMaritalStatus;
	private JTextField txtFinalGrade;
	private JPasswordField pwdPassword;
	
	private AccessDB dbAccess;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateUserMenu frame = new CreateUserMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public CreateUserMenu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 484, 796);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		
		// Alles unter dem Bild, Tabellenlayout mit einer Spalte
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		contentPane.add(pnlButtons, BorderLayout.SOUTH);
		pnlButtons.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblFirstName = new JLabel("Vorname:");
		lblFirstName.setBounds(12, 50, 442, 16);
		contentPane.add(lblFirstName);
		
		JLabel lblPassword = new JLabel("Passwort:");
		lblPassword.setBounds(12, 450, 442, 16);
		contentPane.add(lblPassword);
		
		JLabel lblSurName = new JLabel("Nachname:");
		lblSurName.setBounds(12, 100, 442, 16);
		contentPane.add(lblSurName);
		
		JLabel lblBirthday = new JLabel("Geburtsdatum:");
		lblBirthday.setBounds(12, 150, 442, 16);
		contentPane.add(lblBirthday);
		
		JLabel lblStreet = new JLabel("Stra\u00DFe:");
		lblStreet.setBounds(12, 200, 442, 16);
		contentPane.add(lblStreet);
		
		JLabel lblHouseNumber = new JLabel("Hausnummer:");
		lblHouseNumber.setBounds(12, 250, 442, 16);
		contentPane.add(lblHouseNumber);
		
		JLabel lblPostalCode = new JLabel("Postleitzahl:");
		lblPostalCode.setBounds(12, 300, 442, 16);
		contentPane.add(lblPostalCode);
		
		JLabel lblCity = new JLabel("Stadt:");
		lblCity.setBounds(12, 350, 442, 16);
		contentPane.add(lblCity);
		
		JLabel lblLoginName = new JLabel("Loginname:");
		lblLoginName.setBounds(12, 400, 442, 16);
		contentPane.add(lblLoginName);
		
		JButton btnReady = new JButton("Fertig");
		btnReady.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// Alle eingetragenen Werte Speichern (Zeile 116-145)
				String first_name = txtFirstName.getText();
				String sur_name = txtSurName.getText();
				
				// Geburtsdatum in richtiges Format aendern und dann "speichern"
				String birth = txtBirthday.getText();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
				formatter = formatter.withLocale( Locale.GERMAN );  // Locale gibt die menschliche Sprache f�r die �bersetzung sowie kulturelle Normen fuer Klein-/Gro�buchstaben und Abk�rzungen und dergleichen an. Beispiel: Locale.US oder Locale.GERMAN
				LocalDate birthday = LocalDate.parse(birth, formatter);
				
				String street = txtStreet.getText();
				String house_number = txtHouseNumber.getText();
				String postal_code = txtPostalCode.getText();
				String city = txtCity.getText();
				String loginname = txtLoginName.getText();
				
				// Passwort als String "speichern"
				char[] pass = pwdPassword.getPassword();
				String password = pass.toString();
				
				// Parse String zu int
				String salary = txtSalaryExpectations.getText();
				int salary_expectations = Integer.parseInt(salary);
				
				String marital_status = txtMaritalStatus.getText();
				
				// Parse String zu double
				String grade = txtFinalGrade.getText();
				double final_grade = Double.parseDouble(grade);
				
				// Methode btnReady_Clicked() mit allen Werten aufrufen.
				btnReady_Clicked(first_name, sur_name, birthday, birth, street, house_number, postal_code, city, loginname, password, salary_expectations, salary, marital_status, final_grade, grade);
				
			}
		});
		btnReady.setBounds(176, 696, 132, 40);
		btnReady.setFont(new Font("Tahoma", Font.BOLD, 13));
		contentPane.add(btnReady);
		
		txtLoginName = new JTextField();
		txtLoginName.setText("Hier den Loginnamen einf\u00FCgen.");
		txtLoginName.setBounds(12, 415, 442, 22);
		contentPane.add(txtLoginName);
		txtLoginName.setColumns(10);
		
		txtCity = new JTextField();
		txtCity.setText("Hier die Stadt einf\u00FCgen.");
		txtCity.setBounds(12, 365, 442, 22);
		contentPane.add(txtCity);
		txtCity.setColumns(10);
		
		txtPostalCode = new JTextField();
		txtPostalCode.setText("Hier die Postleitzahl einf\u00FCgen.");
		txtPostalCode.setBounds(12, 315, 442, 22);
		contentPane.add(txtPostalCode);
		txtPostalCode.setColumns(10);
		
		txtHouseNumber = new JTextField();
		txtHouseNumber.setText("Hier die Hausnummer einf\u00FCgen.");
		txtHouseNumber.setBounds(12, 265, 442, 22);
		contentPane.add(txtHouseNumber);
		txtHouseNumber.setColumns(10);
		
		txtStreet = new JTextField();
		txtStreet.setText("Hier die Stra\u00DFe einf\u00FCgen.");
		txtStreet.setBounds(12, 215, 442, 22);
		contentPane.add(txtStreet);
		txtStreet.setColumns(10);
		
		txtBirthday = new JTextField();
		txtBirthday.setText("Hier das Geburtsdatum einf\u00FCgen. (Jahr-Monat-Tag)");
		txtBirthday.setBounds(12, 165, 442, 22);
		contentPane.add(txtBirthday);
		txtBirthday.setColumns(10);
		
		txtSurName = new JTextField();
		txtSurName.setText("Hier den Nachnamen einf\u00FCgen.");
		txtSurName.setBounds(12, 115, 442, 22);
		contentPane.add(txtSurName);
		txtSurName.setColumns(10);
		
		txtFirstName = new JTextField();
		txtFirstName.setText("Hier den Vornamen einf\u00FCgen.");
		txtFirstName.setBounds(12, 65, 442, 22);
		contentPane.add(txtFirstName);
		txtFirstName.setColumns(10);
		
		JLabel lblCreateANew = new JLabel("Create a new user!");
		lblCreateANew.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCreateANew.setBounds(176, 13, 132, 16);
		contentPane.add(lblCreateANew);
		
		JLabel lblSalaryExpectations = new JLabel("Gehaltsvorstellungen:");
		lblSalaryExpectations.setBounds(12, 500, 442, 16);
		contentPane.add(lblSalaryExpectations);
		
		JLabel lblMaritalStatus = new JLabel("Familienstand:");
		lblMaritalStatus.setBounds(12, 550, 442, 16);
		contentPane.add(lblMaritalStatus);
		
		JLabel lblFinalGrade = new JLabel("Abschlussnote:");
		lblFinalGrade.setBounds(12, 600, 442, 16);
		contentPane.add(lblFinalGrade);
		
		txtSalaryExpectations = new JTextField();
		txtSalaryExpectations.setText("Hier die Gehaltsvorstellungen einf\u00FCgen.");
		txtSalaryExpectations.setBounds(12, 515, 442, 22);
		contentPane.add(txtSalaryExpectations);
		txtSalaryExpectations.setColumns(10);
		
		txtMaritalStatus = new JTextField();
		txtMaritalStatus.setText("Hier den Familienstand einf\u00FCgen.");
		txtMaritalStatus.setBounds(12, 565, 442, 22);
		contentPane.add(txtMaritalStatus);
		txtMaritalStatus.setColumns(10);
		
		txtFinalGrade = new JTextField();
		txtFinalGrade.setText("Hier die Abschlussnote einf\u00FCgen.");
		txtFinalGrade.setBounds(12, 615, 442, 22);
		contentPane.add(txtFinalGrade);
		txtFinalGrade.setColumns(10);
		
		pwdPassword = new JPasswordField();
		pwdPassword.setBounds(12, 465, 442, 22);
		contentPane.add(pwdPassword);
		

		
	}
	
	public void btnReady_Clicked(String first_name, String sur_name, LocalDate birthday, String birth, String street, String house_number, String postal_code, String city, String loginname, String password, int salary_expectations, String salary, String marital_status, double final_grade, String grade) {
		
		if (first_name.isEmpty() || sur_name.isEmpty() || birth.isEmpty() || street.isEmpty() || house_number.isEmpty() || postal_code.isEmpty() || city.isEmpty() || loginname.isEmpty() || password.isEmpty() || salary.isEmpty() || marital_status.isEmpty() || grade.isEmpty()) {
			// Fehlermeldung - etwas ist nicht ausgefuellt
			JOptionPane.showMessageDialog(null, "Nicht alle Felder sind ausgef�llt!", "Fehler", JOptionPane.ERROR_MESSAGE);
		} else {
			
			
			// Uebergeben bzw setzten der Werte
			int tempP_user_id = 1;
			User user = new User(tempP_user_id, first_name, sur_name, birthday, street, house_number, postal_code, city, loginname, password, salary_expectations, marital_status, final_grade);
			
			user.setFirst_name(first_name);
			user.setSur_name(sur_name);
			user.setBirthday(birthday);
			user.setStreet(street);
			user.setHouse_number(house_number);
			user.setPostal_code(postal_code);
			user.setCity(city);
			user.setLoginname(loginname);
			user.setPassword(password);
			user.setSalary_expectations(salary_expectations);
			user.setMarital_status(marital_status);
			user.setFinal_grade(final_grade);
			
			// Neuen User erstellen
			UserDB createNewUser = new UserDB(dbAccess);
			createNewUser.createUser(user);
			
		}
		
	}
}
