package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Form_aendern extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_aendern frame = new Form_aendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_aendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 484, 704);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getContentPane().setBackground(Color.RED);
			}
		});
		btnRot.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRot.setBounds(12, 110, 127, 25);
		contentPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getContentPane().setBackground(Color.GREEN);
			}
		});
		btnGrn.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnGrn.setBounds(169, 110, 127, 25);
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getContentPane().setBackground(Color.BLUE);
			}
		});
		btnBlau.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnBlau.setBounds(323, 110, 127, 25);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getContentPane().setBackground(Color.YELLOW);
			}
		});
		btnGelb.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnGelb.setBounds(12, 148, 127, 25);
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getContentPane().setBackground((new Color(0xEEEEEE)));
			}
		});
		btnStandardfarbe.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnStandardfarbe.setBounds(169, 148, 127, 25);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(JColorChooser.showDialog(contentPane, "W�hle Farbe", Color.WHITE));
			}
		});
		btnFarbeWhlen.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnFarbeWhlen.setBounds(323, 148, 127, 25);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblAufgabeHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabeHintergrundfarbe.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeHintergrundfarbe.setBounds(12, 81, 248, 16);
		contentPane.add(lblAufgabeHintergrundfarbe);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblDieserTextSoll.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDieserTextSoll.setBounds(110, 33, 231, 16);
		contentPane.add(lblDieserTextSoll);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeText.setBounds(12, 186, 248, 16);
		contentPane.add(lblAufgabeText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnArial.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnArial.setBounds(12, 215, 127, 25);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSansMs.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnComicSansMs.setBounds(169, 215, 127, 25);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnCourierNew.setBounds(323, 215, 127, 25);
		contentPane.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(12, 253, 438, 22);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnInsLabelSchreiben.setBounds(12, 288, 214, 25);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText("");
			}
		});
		btnTextImLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnTextImLabel.setBounds(236, 288, 214, 25);
		contentPane.add(btnTextImLabel);
		
		JLabel lblAufgabeSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeSchriftfarbe.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeSchriftfarbe.setBounds(12, 326, 248, 16);
		contentPane.add(lblAufgabeSchriftfarbe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.RED);
			}
		});
		btnRot_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRot_1.setBounds(12, 355, 127, 25);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnBlau_1.setBounds(169, 355, 127, 25);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSchwarz.setBounds(323, 355, 127, 25);
		contentPane.add(btnSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeSchriftgre.setBounds(12, 393, 248, 16);
		contentPane.add(lblAufgabeSchriftgre);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblDieserTextSoll.getFont().getSize();
				lblDieserTextSoll.setFont(new Font(lblDieserTextSoll.getFont().getFontName(), Font.PLAIN, groesse + 1));
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 13));
		button.setBounds(12, 422, 214, 25);
		contentPane.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblDieserTextSoll.getFont().getSize();
				lblDieserTextSoll.setFont(new Font(lblDieserTextSoll.getFont().getFontName(), Font.PLAIN, groesse - 1));
			}
		});
		button_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_1.setBounds(236, 422, 214, 25);
		contentPane.add(button_1);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeTextausrichtung.setBounds(12, 460, 248, 16);
		contentPane.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLinksbndig.setBounds(12, 489, 127, 25);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnZentriert.setBounds(169, 489, 127, 25);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRechtsbndig.setBounds(323, 489, 127, 25);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabeProgramm.setBounds(12, 527, 248, 16);
		contentPane.add(lblAufgabeProgramm);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnExit.setBounds(12, 556, 438, 88);
		contentPane.add(btnExit);
	}
}
