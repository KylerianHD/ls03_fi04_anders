package controller;

import model.User;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	public void createUser(User user) {
		
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		
		User currentUser = this.userPersistance.readUser(username);
		
		return currentUser;
	}
	
	public void changeUser(User user) {
		
	}
	
	public void deleteUser(User user) {
		
	}
	
	// Vergleicht �bergebene Daten mit User aus DB.
	public boolean checkPassword(String username, String passwort) {
		
		return this.userPersistance.readUser(username).getPassword().equals(passwort);
	}
}
